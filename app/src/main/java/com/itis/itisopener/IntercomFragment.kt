package com.itis.itisopener

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_intercom.*

/*
    Для данного фрагмента нужны данные
        - Ссылка на поток
        - Квартира
        - Адрес
 */

class IntercomFragment : AppFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_intercom, container, false)

    override val title: String
        get() = getString(R.string.intercom_fragment_title)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonOpen.setOnClickListener {
            it.isEnabled = false
            Handler().postDelayed({
                it.isEnabled = true
            }, 2000)
        }
    }
}