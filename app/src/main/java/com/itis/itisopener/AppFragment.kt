package com.itis.itisopener

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class AppFragment : Fragment() {
    abstract val title: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isAdded) (requireActivity() as AppCompatActivity).supportActionBar?.title = title
    }
}